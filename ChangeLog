  120 Alberto Garcia 2018-02-02
      Update version to 1.1.7

  119 Alberto Garcia 2018-02-02
      Udpate README and add some extra documentation

  118 Alberto Garcia 2018-01-30
      Add examples/run_test.sh to the distribution

  117 Yann Pouillon 2017-12-01
      Had the build system install a pmsl.mk file in org.siesta-project

  116 Alberto Garcia	2017-11-06
      Update version to 1.1.6

  115 Alberto Garcia	2017-11-05
      Update packing list for dist in doc

  114 Alberto Garcia	2017-11-05
      Change m_interp to m_psml_interp

  113 Alberto Garcia	2017-11-05
      Update ford docs. Remove paper pdf

  112 Alberto Garcia	2017-11-05
      Remove old API

  111 Alberto Garcia	2017-11-05
      Update installation instructions

  110 Alberto Garcia	2017-11-05 [merge]
      Merge Yann's autotools
      
      * I had to change a loader option in src/Makefile.am:
      
      -  libpsml_la_LDFLAGS += -Wl,'-expect_unresolved psml_die*'
      +  libpsml_la_LDFLAGS += -expect_unresolved psml_die

  109 Alberto Garcia	2017-07-27 {libpsml-1.1.5}
      Export of ps_real_kind. Simple test. Update psml files and docs
      
      * Update the documentation of the annotation routines
      * Add export of ps_real_kind, and document it.
      * Update the psml files in examples/
      * Add a very simple installation test and reference data
      * Update the release_notes
      
      This is a patch release tagged as libpsml-1.1.5

  108 Alberto Garcia	2017-07-19 {libpsml-1.1.4}
      Release of libpsml-1.1.4
      
      * Update the release_notes
      * Remove fossil file
      * Update README
      
      We follow the patch convention encoded in src/m_psml_core.f90, which
      is also followed by the ps_GetLibPSMLVersion function, so the first
      1.1 release is 1.1.4.

  107 Alberto Garcia	2017-07-18 {libpmsl-1.1-rc4}
      Update draft preprint and CHANGES file

  106 Alberto Garcia	2017-07-18
      Work around missing documentation for aliased types
      
      Added a section in the developer notes to explain the origin
      of ps_annotation_t and ps_radfunc_t.
      
      Updated front matter in top-level libpsml.md

  105 Alberto Garcia	2017-07-18
      Add the missing documentation
      
      Wrote an overview of the functionality, with links to FORD-generated
      interfaces. Note that the interfaces in the code itself are lightly commented,
      originally in a Doxygen interface rougly compatible with FORD. This will be
      done progressively in future revisions of libpsml beyond the release.

  104 Alberto Garcia	2017-07-18 {libpsml-1.1-rc3}
      Remove old example files. Use ESL namespace in normalizer
      
      * Removed fossil files in 'examples'.
      
      * Renamed 'examples/test_dump' to 'normalize'.
        In this file, use the namespace URI
      
        http://esl.cecam.org/PSML/ns/1.1
      
      
        

  103 Alberto Garcia	2017-07-18 [merge]
      Support record-number attribute in <provenance>
      
      * The parser will add the appropriate entry in the
      data structures, and the dump routine will generate
      <provenance> elements with the 'record-number' attribute.
      Wrongly ordered elements in the file will trigger an error.
      
      * Update schema and API documentation in paper.

  102 Alberto Garcia	2017-07-17 {libpsml-1.1-rc2} [merge]
      Treatment of tails in interpolation
       
      * Tail regions might exhibit ringing with the high-order extrapolator.
        Upon parsing, the location of the last "zero" point (scanning
        backwards from the end) is encoded in the radfunc_t structure, and
        used as the effective cutoff point. Only in cases where the first
        "non-zero" is very small and sits at an "elbow" in the data there
        will be a bit of ringing, but it will be confined to the last
        interval.
      
      * The new behavior is configurable with a new routine
      
        ps_SetEvaluatorOptions
      
        which consolidates the setting of debugging, interpolator quality,
        and use of effective range. If procedure pointers are supported by
        the compiler the interpolator itself can be set by this routine.
        
      * Added an option '-t' to examples/show_psml to turn off the
        end-of-range processing. In this case the full range in the PSML
        file data will be used for interpolation.  This program also
        generates "raw" tabular data when in "plot" mode.
        

  101 Alberto Garcia	2017-07-13
      Add 'eref' attribute to slps. Provenance and char length fixes
      
      * Added support for the 'eref' attribute in semilocal potentials.
      
      * Provenance data for child elements was inserted in the wrong place
        in the pseudo-atom-spec hierarchy.
      
      * Increased the length of the character variables in the ps_t type to
        avoid setting a non-zero status flag in xmlf90's 'get_value' for
        long attributes.
      
      * examples/test_dump now inserts a new provenance element when dumping
        a ps_t object read from a PSML 1.0 file.
      
      * If the pre-processor symbol PSML_NO_OLD_API is defined, only the new
        API routines will be compiled in.
      
      * Updated examples/{show_psml,getz} to use only the new API, and
        inserted pre-processor instructions to avoid compiling
        examples/test_psml if the old API compatibility layer is not
        compiled in. Remove outdated programs v10tov11 and dumper.
      
      * Updated the schema and the description paper.

  100 Alberto Garcia	2017-07-07
      Add optional energy_level attribute in the <pswf> element

   99 Alberto Garcia	2017-07-04 {libpsml-1.1-rc1} [merge]
      New modular schema (1.1) and matching API
      
      A new API follows closely the (modular) schema in doc/schema/psml.rnc, which
      has been upgraded to PSML 1.1 after a few clarifications and name changes.
      (The library will also support older PSML 1.0 files).
      
      The documentation is still in progress, but an updated version of the PSML
      paper draft (in doc/paper) contains a functional description of the new routines.

   98 Alberto Garcia	2017-07-04 {libpsml-1.0.5}
      Enlarge size of annotation object if needed
      
      New 'insert' routine with implicit initialization
      and automatic redimensioning of arrays.

   97 Alberto Garcia	2017-06-14 {libpsml-1.0.4} [merge]
      Add support for sets of wavefunctions
      
      The data structures have been updated to mimick
      those for semilocal and nonlocal blocks.

   96 Alberto Garcia	2017-06-14
      Update datatype restrictions in PSML schema
      
      Restrict, when practical, the values of some
      attributes.

   95 Alberto Garcia	2017-06-13
      Update dump interface
      
      Properly dump the vlocal block.
      Export a few more symbols in m_psml to facilitate the working
      of some utility programs.
      
      Added examples/test_dump

   94 Alberto Garcia	2017-06-12
      Process pseudo-wavefunctions in examples/test_psml

   93 Alberto Garcia	2017-05-21
      Update documentation

   92 Alberto Garcia	2017-05-21
      Add more documentation

   91 Alberto Garcia	2017-04-04 {libpsml-1.0.3} [merge]
      Add XML schema. Add documentation skeleton based on FORD
      
      Added a RELAX-NG schema for the XML structure used in PSML.
      (doc/schema)
      
      Prepared a skeleton for the documentation tree based on FORD
      (https://github.com/cmacmackin/ford).

   90 Alberto Garcia	2017-04-04
      Add examples/vlocal_psml

   89 Alberto Garcia	2017-03-01 {libpsml-1.0.2}
      Avoid joint clauses in grid processing
      
      Some versions of the Intel compiler seem to evaluate both clauses
      in an .and. expression, even if the first is .false. The grid 
      processing logic has been made more structured to avoid this.

   88 Alberto Garcia	2017-03-01
      Add subordinate modules to install list
      
      Some versions of the Intel compiler require the subordinate modules
      (those "used" by m_psml.mod) to be present in the
      /path/to/installation/include directory.

   87 Alberto Garcia	2017-01-27 {libpsml-1.0.1}
      New example program 'chlocal_psml'. Updated PSML files
      
      Added a new program in 'examples': chlocal_psml.
      Updated example PSML files in 'examples' to PSML 1.0.
      No changes to the behavior of the library itself

   86 Alberto Garcia	2017-01-17 {libpsml-1.0.0}
      Update version strings. Add version handling routines
      
      * New routines for getting and setting version info.
      * Update paper.
            
      modified:
        doc/paper/header.txt
        doc/paper/psml.tex
        src/m_psml_api.F90
        src/m_psml_core.f90
        src/m_psml_ps_edit.F90

   85 Alberto Garcia	2017-01-13 {libpsml-0.9.6, last-0.9-version}
      Add accessors for core-charge rcore and ncont_derivs
      
      New functions
      
      ps_CoreCharge_NumberOfKeptDerivatives
      ps_CoreCharge_MatchingRadius
      
      They return -1 and -1.0, respectively, if the corresponding
      attributes do not appear in the PSML file.
      
      The test_psml program now returns any annotation in the core charge, and
      the number of continuous derivatives.
      
      Updated the paper.
      
      modified:
        doc/paper/psml.tex
        examples/test_psml.F90
        src/m_psml_api.F90
        src/m_psml_parsing_helpers.F90

   84 Alberto Garcia	2016-12-20 {libpsml-0.9.5}
      Add ps_HasSemilocalPotentials function

   83 Alberto Garcia	2016-12-20
      Add examples/getz
      
      A simple program to output the atomic number, for use in scripts.

   82 Alberto Garcia	2016-07-26
      Remove 'optional' attribute in obj error handler routine
      
      Also, remove the obsolete die routine in examples/psml_die
      
      modified:
        examples/psml_die.F90
        src/basic_type.inc

   81 Alberto Garcia	2016-07-26
      Use psml_die as error handler in the object code
      
      Parametrize the error handler in basic_type.inc, and set it
      to 'psml_die' for this library.
      
      modified:
        src/basic_type.inc

   80 Alberto Garcia	2016-07-12 {libpsml-0.9.4}
      Fix typo in install target of makefile
      
      The typo caused the installation of a .mk with the
      wrong name...
      
      modified:
        src/makefile

   79 Alberto Garcia	2016-07-08 {libpsml-0.9.3}
      Bump version to 0.9.3

   78 Alberto Garcia	2016-07-08
      Add more example psml files. Fix bug in examples/test_psml
      
      Added files processed by Siesta's psop, and a README file.
      
      added:
        examples/80_Hg-siesta-vnl.psml
        examples/Ba.sc-ionic-siesta-vnl.psml
        examples/README
      modified:
        examples/test_psml.F90

   77 Alberto Garcia	2016-07-08
      Update some psml files in 'examples' to version 0.9
      
      removed:
        examples/Test.psml
      modified:
        examples/52_Te_r.psml
        examples/80_Hg.psml
        examples/83_Bi_r.psml
        examples/Ba.sc-ionic.psml
        examples/Fe.spin.psml

   76 Alberto Garcia	2016-07-07 [merge]
      Implement config.sh and VPATH mechanism

   75 Alberto Garcia	2016-07-07
      Enlarge field size in annotation dictionary

   74 Alberto Garcia	2016-07-06
      Minor tweaks to building system
      
      Experimental re-use of xmlf90's fortran.mk.
      
      (+ declare variable in assoc_list.f90)

   73 Alberto Garcia	2016-07-04
      Update to build system: psml.mk file
      
      Handle the xmlf90 dependency in a more robust way.
      
      added:
        src/psml.mk.in
      modified:
        src/fortran.mk
        src/makefile

   72 Alberto Garcia	2016-02-16 [merge]
      Merge papers changes by J. Junquera
      
      - New reference to the Libxml library included in psml.bib
      - New file with an example for the grid element
      - The main paper has been written in Elsevier format,
        some broken links where corrected.
        Some questions by Alberto were answered on the text.

   71 Alberto Garcia	2016-02-02 [merge]
      Remove packaged xmlf90 code. Update grid implementation. Uuids.
      
      * Removed the internal xmlf90 package. The idea is that the xmlf90 library
      is linked in directly.
      
      * The pointer idioms for hierarchical grid handling are not flexible enough.
      Among other things, they do not allow selective removal of blocks. The
      'object' technology has been deployed for them (see src/class_grid.F90).
      
      *  Add support for uuids. Update provenance handling.
      
       -  New accessor function ps_GetUUID and dumper routine ps_SetUUID
          to handle uuids.
        
       -  The insertion of provenance records through ps_AddProvenanceRecord
          has changed: 'creator' and 'date' are passed explicitly. All other
          information is passed as an annotation.
        
       -  Increase length of 'value' files in assoc_list to 120.
        
      * Update doc/paper
      
      * Bug fixes:
      
        - The local-potential element was not treated as able to hold a
          mid-level grid.
      
        - If a block specifies a set string, it is now stored properly in
          the appropriate data section.
      
      * Update INSTALL. Remove obsolete src/README.issues

   70 Alberto Garcia	2016-01-21
      Document latest format changes
      
      * The psml.tex file has been updated to record the latest changes
      regarding the format, including:
      
      - The split of the pseudopotential-operator element
      - The placement of <annotation> and mid-level <grid> elements
      
      * Removed some obsolete hard-wired dimensioning parameters from
        m_psml_core.

   69 Alberto Garcia	2015-12-09
      Proof of concept of editor API
      
      The new routine ps_AddProvenanceRecord takes a ps_t structure
      and an annotation and adds a new provenance record.
      
      added:
        examples/dumper.F90
        src/m_psml_ps_edit.F90
      modified:
        src/m_psml.F90
        src/makefile

   68 Alberto Garcia	2015-12-09
      Make nonlocal-projectors data dynamic
      
      Follow the lines of the semilocal work
      
      
      modified:
        src/m_psml_api.F90
        src/m_psml_core.f90
        src/m_psml_dump.F90
        src/m_psml_parsing_helpers.F90
        src/m_psml_tables.F90

   67 Alberto Garcia	2015-12-09
      Simplify sl_table
      
      There is no independent 'set' array.
      
      modified:
        src/m_psml_api.F90
        src/m_psml_core.f90
        src/m_psml_parsing_helpers.F90
        src/m_psml_tables.F90

   66 Alberto Garcia	2015-12-09
      Add optional argument 'indent' to ps_DumpPSMLFile
      
      modified:
        src/m_psml_dump.F90

   65 Alberto Garcia	2015-12-09
      Remove parsing counters
      
      removed:
        src/m_psml_parsing_counters.F90
      modified:
        src/m_psml_reader.F90
        src/makefile

   64 Alberto Garcia	2015-12-09
      While parsing, preserve ordering of provenance records
      
      
      modified:
        examples/Test.psml
        src/m_psml_parsing_helpers.F90

   63 Alberto Garcia	2015-12-09
      Use CDATA sections for input files
      
      modified:
        src/m_psml_dump.F90

   62 Alberto Garcia	2015-12-09
      Make semilocal-potential data dynamic
      
      Proof of concept. Sub-grids, annotations, etc, can
      now be stored in the semilocal list links.
      
      After parsing, a flat table is generated for the
      accessors.
      
      added:
        src/m_psml_tables.F90
      modified:
        examples/makefile
        src/m_psml_api.F90
        src/m_psml_core.f90
        src/m_psml_dump.F90
        src/m_psml_parsing_helpers.F90
        src/m_psml_reader.F90
        src/makefile

   61 Alberto Garcia	2015-12-08 [merge]
      Sync fixes to sax and wxml libraries from trunk
      
      Process CDATA contents in chunks to avoid overflows
      Fix apos/quot bug. Proper eol's in cdata output
      
      modified:
        src/xmlf90-sax/m_fsm.f90
        src/xmlf90-sax/m_xml_parser.f90
        src/xmlf90-wxml/m_wxml_buffer.f90
        src/xmlf90-wxml/m_wxml_core.f90

   60 Alberto Garcia	2015-12-08
      Split ps_operator element in local potential and projectors
      
      * The <local-potential> element can contain an optional
        <local-charge> element, an annotation, and a grid element.
      
      * The new <nonlocal-projectors> element is really the old
        <projectors>, with the addition of an optional grid.
      
      * Added examples/Test.psml file with the new structure.
      
      added:
        examples/Test.psml
      modified:
        examples/test_psml.F90
        src/m_psml_api.F90
        src/m_psml_core.f90
        src/m_psml_dump.F90
        src/m_psml_parsing_counters.F90
        src/m_psml_parsing_helpers.F90
        src/m_psml_reader.F90

   59 Alberto Garcia	2015-12-07
      Fix apos/quot bug in entity management in xmlf90-wxml

   58 Alberto Garcia	2015-12-05
      Support for several <provenance> elements
      
      * Using a double-linked list, it is easy to
        parse and dump an arbitrary number of <provenance> elements.
      
      modified:
        examples/test_psml.F90
        src/m_psml_api.F90
        src/m_psml_core.f90
        src/m_psml_dump.F90
        src/m_psml_parsing_helpers.F90
            

   57 Alberto Garcia	2015-12-04
      First stage of dynamic allocation in ps_t
      
      * Created m_psml_parsing_counters.F90 to scan the PSML file
        and compute the appropriate sizes. In particular, this is
        useful to support multiple 'provenance' records.
      
      added:
        src/m_psml_parsing_counters.F90
      modified:
        src/m_psml_reader.F90
        src/makefile
            

   56 Alberto Garcia	2015-12-04
      Streamline sax m_reader. Proper header dump
      
      * Integrated the 'm_io' functionality in 'm_reader' in xmlf90-sax.
      
      * Put xc and config_val elements as children of header in dump.
      
      removed:
        src/xmlf90-sax/m_io.f90
      modified:
        src/m_psml_core.f90
        src/m_psml_dump.F90
        src/m_psml_parsing_helpers.F90
        src/xmlf90-sax/m_reader.f90
        src/xmlf90-sax/makefile  

   55 Alberto Garcia	2015-12-04
      (xmlf90-wxml) Write nint(x) if nint(x)==x for reals
      
      modified:
        src/xmlf90-wxml/m_wxml_text.F90
        

   54 Alberto Garcia	2015-12-04
      Add dumper for pseudo-core charge
      
      modified:
        src/m_psml_core.f90
        src/m_psml_dump.F90
      	

   53 Alberto Garcia	2015-12-04
      Add dumpers for xc and config_val
      
      In doing so, add code to parse the (optional) 'type'
      attribute in the libxc_info/functional element.
      
      modified:
        src/m_psml_core.f90
        src/m_psml_dump.F90
        src/m_psml_parsing_helpers.F90
      	

   52 Alberto Garcia	2015-12-03
      Work around gfortran issue in iso_varying_string
      
      Gfortran did not properly handle the zero-length
      array cases in concat_VS_VS.
      
      modified:
        src/iso_varying_string.f90

   51 Alberto Garcia	2015-12-03
      Store 'input-file' in ps as an iso_varying_string. More dumps
      
      * The most convenient "blob" type is that provided by the
      'iso_varying_string' module, which is now part of the distribution.
      
      Parsing is trivially implemented by concatenation. For dumping, the
      size of the standard wxml buffer has been increased to 10000.
      
      * Add provenance and header to the dump subroutine.
      
      added:
        src/iso_varying_string.f90
      modified:
        src/m_psml_core.f90
        src/m_psml_dump.F90
        src/m_psml_parsing_helpers.F90
        src/makefile
        src/xmlf90-wxml/m_wxml_buffer.f90

   50 Alberto Garcia	2015-12-03
      Replace functions returning alloc arrays by subroutines
      
      This is done to work around an unfortunate decision in the Intel
      compiler that turns off by default support for the F2003 feature.
      
      Affected routines are the index-handling functions in m_psml_api, and
      the routine 'set_indexes' in sets_m, used by the dump routine.
      
      modified:
        README
        examples/test_psml.F90
        src/m_psml_api.F90
        src/m_psml_dump.F90
        src/sets_m.F90

   49 Alberto Garcia	2015-12-03
      Add to issues file. Fix clean target in makefile

   48 Alberto Garcia	2015-11-10
      Fix the standalone compilation with static fortran.mk
      
      Simplify the logic of mk file inclusion in the subdirectories.

   47 Alberto Garcia	2015-10-26
      Update wording of LICENSEs

   46 Alberto Garcia	2015-10-26 [merge]
      Merge branch 'dump_psml'
      
      Implement a "dump" feature to produce a PSML file from a ps_t structure.
      Some rough edges remain (see README.issues).

   45 Alberto Garcia	2015-09-14
      Extend the handling of annotations. Version interval.
      
      Annotations can now be associated to various elements, including the
      top element '<psml>', all the grouping elements, the provenance,
      valence configuration, exchange-correlation, and core and valence
      charges. Only the global grid annotation is currently accessible.
      
      The library can now process files in an interval of versions
      (currently [0.80,0.81]).

   44 Alberto Garcia	2015-09-14
      Example file 14_Si.psml with two grids
      
      examples/14_Si.psml, produced by oncvpsp-3.2.2+psml, has
      a global, linear grid with 500 points, and a longer one for
      the valence charge density.

   43 Alberto Garcia	2015-09-14
      Add assoc_list_get_value_by_index to assoc_list module
      
      Turn 'assoc_list_get_value' into a generic overloaded
      interface, with two variants: by key and by index.

   42 Alberto Garcia	2015-09-14
      Add dependency for m_psml in makefile

   41 Alberto Garcia	2015-09-04
      Add support for more mid-level grids (slps, pswfs)
      
      Now the <semilocal-potentials>, <pseudopotential-operator>, and
      <pseudo-wavefunctions> elements can have their own grids.

   40 Alberto Garcia	2015-09-04
      Update grid issues in paper

   39 Alberto Garcia	2015-09-04
      Update authorship info, abstract, and intro

   38 Alberto Garcia	2015-09-02
      Incorporate Javier's changes to psml.tex

   37 Alberto Garcia	2015-09-02
      Add header.txt and provenance.txt in doc/paper

   36 Alberto Garcia	2015-09-02
      New 'paper' and 'schema' subdirs in doc

   35 Alberto Garcia	2015-07-10
      Fix the intent of ps in psml_reader

   34 Alberto Garcia	2015-07-10
      Support systems without proc pointers

   33 Alberto Garcia	2015-06-10
      Fixes for portability

   32 Alberto Garcia	2015-06-10
      Support for external ARCH_MAKE (fix+examples)

   31 Alberto Garcia	2015-06-10
      Support for external ARCH_MAKE in src

   30 Alberto Garcia	2015-04-17
      New introduction for the paper in doc/

   29 Alberto Garcia	2015-04-17
      Add relax-ng schema file stub
      
      Just a few lines. Incomplete.

   28 Alberto Garcia	2015-04-10
      Change format from d to e descriptor in test_psml.f90

   27 Alberto Garcia	2015-04-08
      Fix interpolator initialization to stay within f2003
      
      The ability to initialize procedure pointers to something
      other than null() is a f2008 feature, not yet implemented
      by some compilers. To work around this, a new routine
      set_default_interpolator() has been added to m_interp.f90,
      and a call to it added to the end of the psml reader routine.
      This is rather ugly but unavoidable if we want to maintain
      the user-defined interpolator feature.

   26 Alberto Garcia	2015-04-07
      Use DRH's output format for core charge on linear grid

   25 Alberto Garcia	2015-03-31
      New interpolation features
      
      The interpolator and its quality are user-selectable.
      (See example in examples/test_psml.f90)
      
      By default, this version uses a modified version of
      D.R. Hamann's dpnint, at 7th order. (See module m_interp.f90)
      To recover the old behavior, declare 'interpolate_nr'
      as external in your program, and insert the line
      
       call ps_SetInterpolator(interpolate_nr,2)
      
      before retrieving any interpolated data. The routine
      can be found in examples/interpolate_nr.f90, and implements
      a differences-based fourth-order method as described in
      NumRec.
      
      The order of interpolation, given an interpolator, can be changed easily
      by, for example:
      
       call ps_SetInterpolatorQuality(5)

   24 Alberto Garcia	2015-03-30
      Added hooks to get the raw grid and radial data. Experimental DRH interp
      
      The routines ps_Potential_GetRawData (and similar) will return allocatable
      arrays with the raw grid and data for the radial functions.
      
      Added the routine dnpint by D. R. Hamann to m_interpolation.F90 to test the accuracy
      of my own implementation. Work in progress.

   23 Alberto Garcia	2015-03-24
      Add important KB info to doc/psml.tex

   22 Alberto Garcia	2015-02-18
      Fix bug in processing of unknown elements with radfuncs
      
      - The fix involves checking for all the allowed kinds of situations. It
        might be better to set "radfunc_allowed" under the particular elements.

   21 Alberto Garcia	2015-02-17
      Add J accessors. Final cosmetic format changes. Accessor names.
      
      - Add _J accessors for slps, proj, and pswf
      
      - Use ps_XXXX_YYY format for accessors to make them more uniform
      
      - Require <slps> instead of <vps>
      
      - Use 'seq' attribute instead of 'n' in <proj> elements
      
      - Update format documentation

   20 Alberto Garcia	2015-02-17
      Update documentation for format specification

   19 Alberto Garcia	2015-02-17
      Make set accessors return set codes, not strings. User extension
      
      - The PotentialSet, etc functions now return the standard set codes.
      
      - The function str_of_set is now exported from psml_core to get a
        descriptive string.
      
      - For extensibility, two custom sets, SET_USR1 and SET_USR2, are provided,
        with associated strings "user_extension1" and "user_extension2".
      
      - Allow the reading of "j" attributes for all cases, complaining only
        of their absence for SET_LJ functions.

   18 Alberto Garcia	2015-02-17
      Use simpler set codes, and provide specific accessors
      
      - The set specification is uniform for potentials, projectors, and
        wavefunctions: SET_SREL, SET_NONREL, SET_SO, etc.
      
      - Provide new Number_Of_Potentials, etc functions
      - Provide new Potential_Indexes, etc functions
      
      - Do not export set_code and set_string (for now)

   17 Alberto Garcia	2015-02-16
      Updated the "flavor" handling and allowed for <slps> element
      
      The discussion of the flavor in the documentation for the format
      was not consistent with the code. It has now been updated in both
      places to make it more clear.
      
      Allowed the parsing of documents with <vps> or <slps> as SL potential
      element.

   16 Alberto Garcia	2015-02-16
      More documentation in psml.tex

   15 Alberto Garcia	2015-02-16
      Add doc/psml.tex for documentation of the format>

   14 Alberto Garcia	2015-02-16 [merge]
      Merged branch 'sets'.
      
      - Use a flat database of sl potentials, projectors, and
        ps-wavefunctions, and provide symbolic names for relevant sets to be
        used to create appropriate subsets of indexes.
      
      - Accessors deal with global indexes.
      
      - Version of PSML format bumped up to 0.8:
      
        - New "relativity" attribute in header replaces "relativistic".
      
        - There can be several "<semilocal-potentials>, <projectors>, and
        <pseudo-wave-functions> elements, with an optional "set" attribute.
      
      - New (full) psml examples from oncvpsp code.

   13 Alberto Garcia	2015-02-12
      Proper evaluation of Vlocal beyond the range

   12 Alberto Garcia	2015-02-12
      Added ps_HasPSOperator function

   11 Alberto Garcia	2015-02-11
      Add export for ps_annotation_t

   10 Alberto Garcia	2015-02-11
      Put more comments in test program

    9 Alberto Garcia	2015-02-11
      More diagnostics for xc. Update l field in test files

    8 Alberto Garcia	2015-02-10
      Added support for an arbitrary mix of libxc functionals

    7 Alberto Garcia	2015-02-10
      Add m_getopts.f90 in examples

    6 Alberto Garcia	2015-02-10
      Separated 'core' from 'api'

    5 Alberto Garcia	2015-02-04
      Expand the capabilities of the test program

    4 Alberto Garcia	2015-02-04
      Trap non-positive indexes in sl-potential queries

    3 Alberto Garcia	2015-02-04
      Increase array sizes in parsing data structure
      
      In particular, MAXN_PROJ (max number of projectors) was not
      adequate for some cases.

    2 Alberto Garcia	2015-02-03
      Fix the logic related to the optional V_NL grid
      
      'radfunc' elements below the <pseudopotential-operator> element
      could not access the global grid data.

    1 Alberto Garcia	2015-01-27 {0.7.0}
      Initial git commit at version 0.7

Use --include-merged or -n0 to see merged revisions.
