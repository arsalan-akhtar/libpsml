# -*- Automake -*-
#
# Makefile for the LibPSML package
#
# Copyright (C) 2017 Yann Pouillon
#
# This file is part of the LibPSML software package. For license information,
# please see the COPYING file in the top-level directory of the source
# distribution.
#

                    # ------------------------------------ #

#
# Main source files
#

# Common source files
psml_srcs = \
  assoc_list.F90 \
  class_grid.F90 \
  external_interfaces.F90 \
  iso_varying_string.F90 \
  m_aux_aliases.f90 \
  m_psml_interp.F90 \
  m_psml.F90 \
  m_psml_api.F90 \
  m_psml_core.F90 \
  m_psml_dump.F90 \
  m_psml_parsing_helpers.F90 \
  m_psml_ps_edit.F90 \
  m_psml_reader.F90 \
  m_psml_tables.F90

# Fortran includes
EXTRA_DIST = \
  basic_type.inc \
  psml_die_for_tests.F90

# Fortran modules
# Note: never expect they will have the same name as their source
MODEXT = @ax_cv_f90_modext@
if F90_MOD_UPPERCASE
psml_f03_mods = \
  ASSOC_LIST.$(MODEXT) \
  CLASS_GRID.$(MODEXT) \
  EXTERNAL_INTERFACES.$(MODEXT) \
  ISO_VARYING_STRING.$(MODEXT) \
  M_AUX_ALIASES.$(MODEXT) \
  M_PSML_INTERP.$(MODEXT) \
  M_PSML.$(MODEXT) \
  M_PSML_API.$(MODEXT) \
  M_PSML_CORE.$(MODEXT) \
  M_PSML_DUMP.$(MODEXT) \
  M_PSML_PARSING_HELPERS.$(MODEXT) \
  M_PSML_PS_EDIT.$(MODEXT) \
  M_PSML_READER.$(MODEXT) \
  M_PSML_TABLES.$(MODEXT)
else
psml_f03_mods = \
  assoc_list.$(MODEXT) \
  class_grid.$(MODEXT) \
  external_interfaces.$(MODEXT) \
  iso_varying_string.$(MODEXT) \
  m_aux_aliases.$(MODEXT) \
  m_psml_interp.$(MODEXT) \
  m_psml.$(MODEXT) \
  m_psml_api.$(MODEXT) \
  m_psml_core.$(MODEXT) \
  m_psml_dump.$(MODEXT) \
  m_psml_parsing_helpers.$(MODEXT) \
  m_psml_ps_edit.$(MODEXT) \
  m_psml_reader.$(MODEXT) \
  m_psml_tables.$(MODEXT)
endif

# Libraries to install
lib_LTLIBRARIES = libpsml.la

libpsml_la_SOURCES = $(psml_srcs)
libpsml_la_LDFLAGS = -version-info 0:0:0
libpsml_la_LDFLAGS += -expect_unresolved psml_die

# Fortran modules to install
f03moddir = $(includedir)
install-data-local:
	$(INSTALL) -d -m 755 $(DESTDIR)$(f03moddir)
	$(INSTALL) -m 644 $(psml_f03_mods) $(DESTDIR)$(f03moddir)

uninstall-local:
	cd $(DESTDIR)$(f03moddir) && rm -f $(psml_f03_mods)

# Local cleaning
CLEANFILES = $(psml_f03_mods)

                    # ------------------------------------ #

# Basic tests
check_PROGRAMS = test_assoc

test_assoc_SOURCES = test_assoc.F90
test_assoc_LDADD = libpsml.la psml_die_for_tests.$(OBJEXT)

# List of test programs (XFAIL = supposed to fail)
TESTS = test_assoc

                    # ------------------------------------ #

# Explicit dependencies within LibPSML
# Note: this is needed because of Fortran
class_grid.$(LTOBJEXT): \
  assoc_list.$(LTOBJEXT)

m_aux_aliases.$(LTOBJEXT): \
  m_psml_core.$(LTOBJEXT)

m_psml.$(LTOBJEXT): \
  assoc_list.$(LTOBJEXT) \
  m_psml_api.$(LTOBJEXT) \
  m_psml_core.$(LTOBJEXT) \
  m_psml_dump.$(LTOBJEXT) \
  m_psml_ps_edit.$(LTOBJEXT) \
  m_psml_reader.$(LTOBJEXT)

m_psml_api.$(LTOBJEXT): \
  assoc_list.$(LTOBJEXT) \
  class_grid.$(LTOBJEXT) \
  external_interfaces.$(LTOBJEXT) \
  m_aux_aliases.$(LTOBJEXT) \
  m_psml_interp.$(LTOBJEXT) \
  m_psml_core.$(LTOBJEXT)

m_psml_core.$(LTOBJEXT): \
  class_grid.$(LTOBJEXT)

m_psml_dump.$(LTOBJEXT): \
  assoc_list.$(LTOBJEXT) \
  class_grid.$(LTOBJEXT) \
  external_interfaces.$(LTOBJEXT) \
  iso_varying_string.$(LTOBJEXT) \
  m_psml_core.$(LTOBJEXT)

m_psml_parsing_helpers.$(LTOBJEXT): \
  assoc_list.$(LTOBJEXT) \
  class_grid.$(LTOBJEXT) \
  external_interfaces.$(LTOBJEXT) \
  iso_varying_string.$(LTOBJEXT) \
  m_psml_core.$(LTOBJEXT)

m_psml_ps_edit.$(LTOBJEXT): \
  assoc_list.$(LTOBJEXT) \
  external_interfaces.$(LTOBJEXT) \
  m_psml_core.$(LTOBJEXT)

m_psml_reader.$(LTOBJEXT): \
  external_interfaces.$(LTOBJEXT) \
  m_psml_interp.$(LTOBJEXT) \
  m_psml_core.$(LTOBJEXT) \
  m_psml_parsing_helpers.$(LTOBJEXT) \
  m_psml_tables.$(LTOBJEXT)

m_psml_tables.$(LTOBJEXT): \
  m_psml_core.$(LTOBJEXT)
