title: Developer Notes

# Abstract types

* The main type of the library is [[ps_t(type)]].

* The `ps_annotation_t` type is actually an alias for [[assoc_list_t]].

* The `ps_radfunc_t` type is actually an alias for [[m_psml_core(module):radfunc_t(type)]]

