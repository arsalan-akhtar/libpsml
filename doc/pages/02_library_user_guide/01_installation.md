title: Installation

## Pre-requisites

* The [libPSML library](http://launchpad.net/libpsml) itself.

* The [xmlf90 library](http://launchpad.net/xmlf90)
  Follow the instructions in the package to compile it.

## Installation of libPSML

```
    ./configure --prefix=/path/to/installation --with-xmlf90=/path/to/xmlf90
    make
    make check
    make install
```

## Test programs

Go into the subdirectory `examples`. In it you can find, among others:

* [[normalize(program)]]:  A program to parse a PSML file and dump the resulting `ps`
object.

* [[show_psml(program)]]: A program to parse a PSML file and extract
  various kinds of information, with optional evaluation of radial
  functions on a linear grid for later plotting.

## Compiling user programs

After installation, the appropriate modules and library files should
already be in `$PREFIX/include` and `$PREFIX/lib`, respectively.

The basic idiom is to provide the basic Fortran building rules, and
to insert the paths to the libpsml (and xmlf90) modules and libraries.








