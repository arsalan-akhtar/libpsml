title: libPSML User Guide

We provide a companion library to the PSML format, libPSML, that
provides transparent parsing and data extraction from PSML files, as
well as basic editing and data conversion capabilities.

The library is built around a data structure of type [[ps_t(type)]]
that maps the information in a PSML file. Instances of this
structure are populated by PSML parsers, processed by
intermediate utility programs, and used as handles for information
retrieval by client codes through accessor routines. The library
provides, in essence:

*  A routine to parse a PSML file and produce a `ps`
  object of type [[ps_t(type)]].
*  A routine to dump the information in a `ps` object to a
  PSML file.
*  Accessor routines to extract information from `ps`
  objects.
*  Some setter routines to insert specific blocks of information
  into `ps` objects. These might be used by intermediate
  processors or by high-level parsers.

The library is written in modern Fortran and provides a
high-level Fortran interface. A C/C++ interface is in preparation.

The basic modus operandi is to parse a PSML file, storing the information
in an abstract handle [[ps_t(type)]], and then to query the handle to
extract the relevant data.

```fortran
use m_psml

type(psml_t) :: ps
call psml_reader("input.psml",ps,debug=.false.)
...
! Set up a grid of our own choosing
npts = 400; delta = 0.01  
allocate(r(npts))
do ir = 1, npts
   r(ir) = (ir-1)*delta
enddo

! Query existing potentials and get values
call ps_Potential_Filter(ps,set=SET_SREL,indexes=idx,number=npots)
do i = 1, npots
   call ps_Potential_Get(ps,idx(i),l=l,n=n,rc=rc)
   ...
   do ir = 1, npts
      val = ps_Potential_Value(ps,idx(i),r(ir))
      ...
   enddo
enddo
...
call ps_destroy(ps)
```
