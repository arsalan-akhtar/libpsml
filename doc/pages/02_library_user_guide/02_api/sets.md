title: Sets of potentials, projectors, and pseudo-wave-functions

A given pseudopotential-generator code might offer several possible
levels of theory, and output different sets of magnitudes accordingly.

The `set` attribute in several API routines allows the handling of
various sets of semilocal pseudopotentials, projectors, and
pseudo-wave-functions. Its value is normalized as follows, depending
on the type of calculation generating the pseudopotential and the way
in which the code chooses to present the results. (First, the symbolic
name used in the API is given, followed by the value of the attribute in
the PSML file)

* (`SET_NONREL`, `non_relativistic`) for the non-relativistic, non-spin-DFT
case.

* (`SET_SREL`,`scalar_relativistic`) if the calculation is
scalar-relativistic, or if it is fully relativistic and an set of `lj`
potentials averaged over `j` is provided.

* (`SET_SO`, `spin_orbit`) if a
fully relativistic code provides this combination of `lj` potentials.

* (`SET_LJ`,`lj`) for a fully relativistic calculation with straight output
of the $lj$ channels.

* (`SET_UP`,`up`)  and (`SET_DOWN`,`down`), for a spin-DFT
calculation with straight output of the spin channels.

* (`SET_SPINAVE`,`spin_average`) for the spin-DFT case when the generation code
outputs a population-averaged pseudopotential.

* (`SET_SPINDIFF`,`spin_difference`) for the spin-DFT case when the generation code
outputs this (rare) combination.

Note that a given code might choose to output its semilocal-potential
information in two different forms (say, as scalar-relativistic and
spin-orbit combinations plus the `lj` form). The format allows this,
although in this particular case the information can easily be
converted from the `lj` form to the other by client programs.

For extensibility, the format allows two more values for the `set`
attribute, (`SET_USER1`,`user_extension1`) and (`SET_USER2`,
`user_extension2`), which can in principle be used to store custom
information while maintaining structural and operative compatibility
with the format.

The wildcard specification `SET_ALL` can be used to represent the
union of all possible sets.