title: Databases of PSML files

The [Pseudo-Dojo](http://www.pseudo-dojo.org) project maintains a
database of pseudopotentials generated
with the oncvpsp code, and provides extensive information on their
performance on several tests. One of the formats supported is PSML.

More information about the project is available as a
[preprint](https://arxiv.org/abs/1710.10138).
