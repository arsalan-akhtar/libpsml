title: PSML description

PSML is a file format for norm-conserving
pseudopotential data which is designed to encapsulate as much as
possible the concepts in the domain’s ontology. For example,
the format, together with its supporting processing library, offers a
very good approximation to an abstract mathematical function (an object
which produces a value from its input). PSML also provides a rich
set of metadata to document the provenance (i.e., the generation
history) and the generation conditions and flavor of the
pseudopotential. The format is based on XML, and it is by
nature extensible, offering a basic set of elements and a number of
extra features that can be exploited for sophisticated uses or future
improvements.

As first steps in the implementation of the PSML vision, we have modified two
different atomic pseudopotential generation codes to generate PSML
files, and interfaced libPSML to two electronic-structure programs.

Example PSML files can be found in the `examples` directory of the
libPSML distribution, and also in the [databases](./databases.html) of
pseudopotentials that support the format.

* [Pseudopotential generators supporting PSML](./01_generators/index.html)
* [First-principles codes able to read PSML files](./02_clients/index.html)
* [Schema for PSML](./schema.html)
* [Databases of PSML files](./databases.html)

